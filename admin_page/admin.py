from django.contrib import admin
from .models import Devices, Containers, SensorData, Sensors

admin.site.register(Containers)
admin.site.register(Devices)
admin.site.register(Sensors)
admin.site.register(SensorData)