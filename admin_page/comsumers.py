from channels.generic.websocket import WebsocketConsumer
import json
from asgiref.sync import async_to_sync

class DataConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = "channel1"
        print('connect...')
        self.accept()
        async_to_sync(self.channel_layer.group_add)("channel1", self.channel_name)
        print(f"Add {self.channel_name} channel to data's group")

    def receive(self, text_data):
        print('receive', text_data)

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)("channel1", self.channel_name)
        print(f"Remove {self.channel_name} channel from data's group")

    def render_html(self, event):
        self.send(text_data=json.dumps({"html": event['message']}))
