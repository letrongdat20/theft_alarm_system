from django import forms
from admin_page.models import Containers
from admin_page.models import Devices


class DeviceForm(forms.ModelForm):
    class Meta:
        model = Devices
        fields = ('name', 'device_code', 'ampe', 'von', 'size', 'domain_upper',
                  'domain_lower', 'alert_threshold')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'device_code': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'ampe': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'von': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'size': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'domain_upper': forms.NumberInput(attrs={'class': 'custom-form-control'}),
            'domain_lower': forms.NumberInput(attrs={'class': 'custom-form-control'}),
            'alert_threshold': forms.NumberInput(attrs={'class': 'custom-form-control'}),
        }


class ContainerForm(forms.ModelForm):
    class Meta:
        model = Containers
        fields = ('name', 'container_code', 'type', 'position')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'container_code': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'type': forms.TextInput(attrs={'class': 'custom-form-control'}),
            'position': forms.TextInput(attrs={'class': 'custom-form-control'})
        }
