import datetime
from django.db import models


class Containers(models.Model):
    id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    container_code = models.CharField(max_length=100, unique=True)
    status = models.IntegerField()
    create_at = models.DateTimeField(default=datetime.datetime.now())
    update_at = models.DateTimeField(default=datetime.datetime.now())

class Devices(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    device_code = models.CharField(max_length=50)
    ampe = models.CharField(max_length=20)
    von = models.CharField(max_length=20)
    size = models.CharField(max_length=20)
    alert_threshold = models.IntegerField(default=0)
    domain_upper = models.IntegerField(default=0)
    domain_lower = models.IntegerField(default=0)
    
class Sensors(models.Model):
    id = models.AutoField(primary_key=True)
    position = models.IntegerField()
    device = models.ForeignKey(Devices, on_delete=models.CASCADE)
    container = models.ForeignKey(Containers, on_delete=models.CASCADE)
    create_at = models.DateTimeField(default=datetime.datetime.now())
    
    class Meta:
        unique_together = (('container', 'position'),)
    
class SensorData(models.Model):
    id = models.AutoField(primary_key=True)
    sensor = models.ForeignKey(Sensors, on_delete=models.DO_NOTHING)
    data = models.CharField(max_length=100)
    update_at = models.DateTimeField(default=datetime.datetime.now())
    