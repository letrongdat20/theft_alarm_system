from django.urls import path
from admin_page.comsumers import DataConsumer

websocket_urlpatterns = [
	path('get-data/', DataConsumer.as_asgi()),
]