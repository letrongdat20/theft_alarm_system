var socket = new WebSocket('ws://localhost:8000/get-data/')

socket.onopen = function(e) {
    console.log("open", e);
}
socket.onmessage = function(e) {
    const data = JSON.parse(e.data);
    console.log(data);
    $('body').append(data.html);
}
socket.onerror = function(e) {
    console.log("error", e);
}
socket.onclose = function(e) {
    console.log("close", e);
}