from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('create-data', views.create_data, name='create_sameple_data'),
    path('container/list', views.container_list, name='container_list'),
    path('container/init', views.container_info, name='container_info'),
    path('container/update', views.update_container, name='update_container'),
    path('device/list', views.setting_view, name="device_list"),
    path('device/init', views.divice_info, name="device_info"),
    path('device/update', views.setting, name="update_device"),
    path('', views.dashboard, name="dashboard"),
    path('publish',views.publish, name="publish"),
    path('test', views.test, name='test'),
    path("container/regis", views.regis, name="regis")
]