import json
from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from admin_page.forms import DeviceForm, ContainerForm
from admin_page.models import Containers, Devices, Sensors, SensorData
from channels.layers import get_channel_layer
from django.views.decorators.csrf import csrf_exempt
from asgiref.sync import async_to_sync
from django.db import transaction


def create_container(type, name, position, container_code):
    container = Containers()
    container.name = name
    container.position = position
    container.type = type
    container.status = 3
    container.container_code = container_code
    container.save()


def create_device(name,
                  device_code,
                  ampe,
                  von,
                  size,
                  alert_threshold,
                  domain_lower,
                  domain_upper):
    device = Devices()
    device.name = name
    device.device_code = device_code
    device.ampe = ampe
    device.von = von
    device.size = size
    device.alert_threshold = alert_threshold
    device.domain_lower = domain_lower
    device.domain_upper = domain_upper
    device.save()


def create_data(request):
    create_container("Container", "Container 1",
                     "Edinburgh", "ct001")
    create_container("Container", "Container 2",
                     "Tokyo", "ct002")
    create_container("ATM", "ATM Duy Tan",
                     "so 1 p.Duy Tan q.Cau Giay tp.Ha Noi", "atm001")
    create_container("ATM", "Container Bach Khoa",
                     "so 1 p.Bach Khoa q.Hai Ba Trung tp.Ha Noi", "atm002")
    create_device(name="Cảm biến nghiêng SW520", device_code="SW520",
                  ampe="10 mA", von="3-12 VDC", size="5.2*11.5 mm", alert_threshold=0, domain_upper=1, domain_lower=0)
    create_device(name="Cảm biến rung digital SW-420", device_code="SW-420",
                  ampe="15 mA", von="3.3-5 V", size="32x14 mm", alert_threshold=0, domain_lower=0, domain_upper=1)
    create_device(name="cảm biến rung analog SW-1801P", device_code="SW-1801P",
                  ampe="10 mA", von="3-5 VDC", size="32x14 mm", alert_threshold=0, domain_lower=0, domain_upper=1024)
    return JsonResponse({'message': 'oke'})


def container_list(request):
    data = Containers.objects.all().order_by('-status')
    return render(request, "admin_page/container-list.html", {"data": data})

@csrf_exempt
def dashboard(request):
    if request.method == "GET":
        sensor_datas = SensorData.objects.all().order_by("-update_at")
        datas = list()
        for sensor_data in sensor_datas:
            data = dict()
            data['id'] = sensor_data.sensor.container.id
            data['container'] = sensor_data.sensor.container.name
            data['sensor'] = sensor_data.sensor.device.name
            data['value'] = sensor_data.data
            data['range'] = str(sensor_data.sensor.device.domain_lower) + \
                ' - ' + str(sensor_data.sensor.device.domain_upper)
            data['update_at'] = sensor_data.update_at
            datas.append(data)
        return render(request, "admin_page/dashboard.html", {'data': datas})
    else:
        id = request.POST.get('id')
        if id.isnumeric():
            sensor_datas = SensorData.objects.filter(sensor__container__id=id).order_by("-update_at")
        else:
            sensor_datas = SensorData.objects.all().order_by("-update_at")
        datas = list()
        for sensor_data in sensor_datas:
            data = dict()
            data['id'] = sensor_data.sensor.container.id
            data['container'] = sensor_data.sensor.container.name
            data['sensor'] = sensor_data.sensor.device.name
            data['value'] = sensor_data.data
            data['range'] = sensor_data.sensor.domain_lower + \
                ' - ' + sensor_data.sensor.domain_upper
            data['update_at'] = sensor_data.update_at
            datas.append(data)
        return render(request, "admin_page/includes/data-table.html", {'data': datas})

def container_info(request):
    if request.method == "GET":
        id = request.GET.get('id')
        if id:
            container = Containers.objects.get(pk=id)
            form = ContainerForm(instance=container)
            return render(request, "admin_page/container.html", {'form': form, 'id': id})
        else:
            pass
    else:
        pass


def update_container(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        if id:
            container = Containers.objects.get(pk=id)
            form = ContainerForm(request.POST, instance=container)
            if form.is_valid():
                container = form.save(commit=False)
                container.save()
                return redirect('container_list')
            else:
                return render(request, "admin_page/container.html", {'form': form, 'id': id})
        else:
            pass
    else:
        pass


def divice_info(request):
    if request.method == "GET":
        id = request.GET.get('id')
        if id:
            device = Devices.objects.get(pk=id)
            form = DeviceForm(instance=device)
            return render(request, "admin_page/device.html", {'form': form, 'id': id})
        else:
            pass
    else:
        pass


def setting(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        if id:
            device = Devices.objects.get(pk=id)
            form = DeviceForm(request.POST, instance=device)
            if form.is_valid():
                device = form.save(commit=False)
                device.save()
                return redirect('device_list')
            else:
                return render(request, "admin_page/container.html", {'form': form, 'id': id})
        else:
            pass
    else:
        pass


def setting_view(request):
    data = Devices.objects.all().order_by('-id')
    return render(request, "admin_page/setting.html", {"data": data})


@csrf_exempt
@transaction.atomic
def publish(request):
    if request.method != "POST":
        return JsonResponse({'mess': "no such API"})
    dict_data = json.loads(request.body)
    container_code = dict_data['mac']
    sid = transaction.savepoint()
    try:
        container = Containers.objects.get(container_code=container_code)
    except:
        return JsonResponse({'mess': "No such container"})
    alarm = None
    for data in dict_data['sensor']:
        position = data['position']
        try:
            sensor = Sensors.objects.get(
                container__id=container.id, position=position)
            sensor_data = SensorData()
            sensor_data.sensor = sensor
            sensor_data.data = data['value']
            sensor_data.save()
            if data['value'] > sensor.device.alert_threshold:
                alarm = sensor_data.id
        except:
            transaction.savepoint_rollback(sid)
            return JsonResponse("Invalid sensor")
    # preprocess(data)
    if alarm is not None:
        container.status = 2
        container.save()
        sendData(alarm)
    else:
        container.status = 1
        container.save()
    return JsonResponse({'mess': "success"})


def sendData(id):
    sensor_data = SensorData.objects.get(pk=id)
    data = dict()
    data['container'] = sensor_data.sensor.container.name
    data['sensor'] = sensor_data.sensor.device.name
    data['value'] = sensor_data.data
    data['device'] = sensor_data.sensor.device.name
    print(data);
    html_users = render_to_string(
        "admin_page/includes/alarm.html", {'data': data})
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "channel1", {"type": "render_html", 'message': html_users})


@csrf_exempt
def test(request):
    data = json.loads(request.body)
    print(data)
    return JsonResponse({"status": "200", 'message': 'oke'})


@csrf_exempt
@transaction.atomic
def regis(request):
    dict_data = json.loads(request.body)
    container_code = dict_data['mac']
    res = dict()
    res['mac'] = container_code
    sensor_list = list()
    sid = transaction.savepoint()
    try:
        container = Containers.objects.get(container_code=container_code)
        for device_data in dict_data['sensor']:
            try:
                device = Devices.objects.get(device_code=device_data['model'])
                sensor_res = dict()
                sensor_res['position'] = device_data['position']
                sensor_res['model'] = device_data['model']
                sensor_res['threshold'] = device.alert_threshold
                sensor_list.append(sensor_res)
            except:
                transaction.savepoint_rollback(sid)
                return JsonResponse({"mes": "No such device"})
    except:
        container = Containers()
        container.container_code = container_code
        container.name = "not set"
        container.position = "not set"
        container.type = "not set"
        container.status = 1
        container.save()
        for device_data in dict_data['sensor']:
            try:
                device = Devices.objects.get(device_code=device_data['model'])
                sensor_res = dict()
                sensor_res['position'] = device_data['position']
                sensor_res['model'] = device_data['model']
                sensor_res['threshold'] = device.alert_threshold
                sensor_list.append(sensor_res)
                new_sensor = Sensors()
                new_sensor.container = container
                new_sensor.device = device
                new_sensor.position = device_data['position']
                new_sensor.save()
            except:
                transaction.savepoint_rollback(sid)
                return JsonResponse({"mes": "No such device"})
            
    res['sensor'] = sensor_list
    return JsonResponse(res)
