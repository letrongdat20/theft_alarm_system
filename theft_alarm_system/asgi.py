import os
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application
import admin_page.routing
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'theft_alarm_system.settings')

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": AuthMiddlewareStack(
        URLRouter(
            admin_page.routing.websocket_urlpatterns
        )
    )
})
